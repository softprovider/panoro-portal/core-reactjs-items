//= Functions & Modules
// Others
import jsYaml from 'js-yaml';
import { mkdir, readFile, writeFile } from 'fs/promises';
import { join, dirname, relative } from 'path';
import { emptyDir } from 'fs-extra';

const SETTINGS_FILEPATH = join(__dirname, 'settings.yml');
const SOURCE_DIRPATH = join(__dirname, 'src', 'data');

type PackageResolver = {
    import: string;
    resolves: { [key: string]: string };
};

function createImportFromFileEntry(fileName: string, fileEntry: { [key: string]: any }): string {
    return `import ${fileEntry.import && fileEntry.import !== 'default' ? `{ ${fileEntry.import} }` : 'default'} from "./${fileName}";\n`;
}

function createExportFromFileEntry(fileName: string, fileEntry: { [key: string]: any }, basePath: string = ''): string {
    return `export ${fileEntry.import && fileEntry.import !== 'default' ? `{ ${fileEntry.import} }` : `{ default as ${fileName} }`} from "${
        basePath == '' ? `./${fileName}` : `${basePath}/${fileName}`
    }";\n`;
}

function transformFile(fileData: string, transformSettings: any): string {
    for (let i = 0, length = transformSettings.length; i < length; ++i) {
        let command = transformSettings[i];

        if (command.deleteImport) {
            const matches = fileData.matchAll(new RegExp(`import (.*?) from ["']${command.deleteImport}["'];`, 'gm'));
            for (const match of matches) {
                fileData = fileData.substring(0, match.index) + fileData.substring(match.index + match[0].length);
            }
        }

        if (command.replace) {
            fileData = fileData.replace(new RegExp(command.replace.from, 'gm'), command.replace.to);
        }
    }

    return fileData;
}

function resolveImports(fileData: string, fileName: string, files: { [key: string]: any }, packagesResolvers: PackageResolver[]): string {
    const matches = fileData.matchAll(new RegExp(`import (.*?) from ["'](.*?)["'];`, 'gm'));
    for (const match of matches) {
        let importFilePath = match[2];
        if (importFilePath.startsWith('.')) {
            importFilePath = join(__dirname, dirname(files[fileName].from), `${importFilePath}.ts`);

            let foundFileName: string;
            for (const otherFileName in files) {
                const otherImportFilePath = join(__dirname, files[otherFileName].from);

                if (relative(importFilePath, otherImportFilePath).length == 0) {
                    foundFileName = otherFileName;
                    break;
                }
            }

            if (!foundFileName) {
                throw new Error(`Could not find import file "${importFilePath}" in the given files`);
            }

            fileData = fileData.replace(match[2], `./${foundFileName}`);
        } else {
            for (const packageResolver of packagesResolvers) {
                if (importFilePath == packageResolver.import) {
                    if (match[1][0] == '{') {
                        const importNames = match[1]
                            .substring(1, match[1].length - 1)
                            .split(',')
                            .map((importName) => importName.trim());

                        let newImports = '';
                        for (const importName of importNames) {
                            const resolvedFileName = packageResolver.resolves[importName];
                            if (!resolvedFileName) {
                                throw new Error(
                                    `Cannot resolve package of "${importFilePath}" because is importing "${importName}" but the resolver for "${importName}" is missing from "resolvers"`
                                );
                            }

                            newImports += createImportFromFileEntry(resolvedFileName, files[resolvedFileName]);
                        }

                        fileData = fileData.replace(match[0], newImports);
                    } else {
                        if (!packageResolver.resolves.default) {
                            throw new Error(
                                `Cannot resolve package of "${importFilePath}" because is importing a default but the default resolver is missing from "resolvers"`
                            );
                        }

                        // TODO
                    }
                }
            }
        }
    }

    return fileData;
}

async function run() {
    await emptyDir(join(__dirname, 'src'));
    await mkdir(SOURCE_DIRPATH);

    const settings: any = jsYaml.load(await readFile(SETTINGS_FILEPATH, 'utf8'));

    const fileNames = Object.keys(settings.files);

    let indexFileData = '';

    await Promise.all(
        fileNames.map(async (fileName) => {
            const fileSettings = settings.files[fileName];

            let fileData = await readFile(join(__dirname, fileSettings.from), 'utf8');

            if (fileSettings.transform) {
                for (let i = 0, length = fileSettings.transform.length; i < length; ++i) {
                    fileData = transformFile(fileData, settings.transforms[fileSettings.transform]);
                }
            }

            fileData = resolveImports(fileData, fileName, settings.files, settings.packagesResolvers);

            await writeFile(join(SOURCE_DIRPATH, `${fileName}.ts`), fileData, 'utf8');

            indexFileData += createExportFromFileEntry(fileName, settings.files[fileName], './data');
        })
    );

    await writeFile(join(__dirname, 'src', 'index.ts'), indexFileData, 'utf8');
}

run();
