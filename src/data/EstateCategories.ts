export enum EstateCategories {
    APARTAMENTE = 'apartamente',
    CASE = 'case',
    BIROURI = 'birouri',
    SPATII_COMERCIALE = 'spatiiComerciale',
    SPATII_INDUSTRIALE = 'spatiiIndistruale',
    TERENURI = 'terenuri',
}
