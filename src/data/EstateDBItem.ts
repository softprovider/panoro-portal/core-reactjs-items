//= Functions & Modules
// Others


//= Structures & Data
// Own
import { EstateDBFields } from './EstateDBFields';
import { EstateTransactionTypes } from './EstateTransactionTypes';
import { EstateLocalizationItem } from './EstateLocalizationItem';
// Others
import { EstateCategories } from "./EstateCategories";


export interface EstateDBItem {
    [EstateDBFields.ID]: string;
    [EstateDBFields.CODE]: string;
    [EstateDBFields.CHARACTERISTICS]: { [key: string]: any };
    [EstateDBFields.TIME_CREATED]: Date;
    [EstateDBFields.TIME_LAST_UPDATE]: Date;
    [EstateDBFields.USERID]: string;
    [EstateDBFields.CATEGORY]: EstateCategories;
    [EstateDBFields.TRANSACTION_TYPE]: EstateTransactionTypes;
    [EstateDBFields.LOCALIZATION]: EstateLocalizationItem;
    [EstateDBFields.MISSING_FIELDS]: any;
    [EstateDBFields.NEIGHBORHOODID]?: string;
    [EstateDBFields.CITYID]: string;
    [EstateDBFields.COUNTYID]: string;
    [EstateDBFields.COUNTRYID]: string;
    [EstateDBFields.IS_ACTIVE]: boolean;
    [EstateDBFields.STREET]: string;
    [EstateDBFields.STREET_NUMBER]: string;
    [EstateDBFields.APARTMENT_NUMBER]?: string;
    [EstateDBFields.TITLE]: string;
    [EstateDBFields.DESCRIPTION]: string;
}
