export enum LocationLevels {
    COUNTRY = 0,
    COUNTY = 1,
    CITY = 2,
    NEIGHBORHOOD = 3,
}

