//= Functions & Modules
// Others


//= Structures & Data
// Own
import { LocationDBFields } from './LocationDBFields';
import { LocationLevels } from './LocationLevels';

export interface LocationDBItem {
    [LocationDBFields.ID]: string;
    [LocationDBFields.NAME]: string;
    [LocationDBFields.LEVEL]: LocationLevels;
    [LocationDBFields.PARENT_ID]?: string;
    [LocationDBFields.URL_NAME]: string;
}
