export enum LocationDBFields {
    ID = '_id',
    NAME = 'name',
    PARENT_ID = 'parentID',
    LEVEL = 'level',
    URL_NAME = 'urlName',
}
