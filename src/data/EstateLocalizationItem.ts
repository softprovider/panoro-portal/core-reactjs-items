//= Structures & Data
// Own
import { EstateLocalizationFields } from './EstateLocalizationFields';

export interface EstateLocalizationItem {
    [EstateLocalizationFields.LATITUDE]: number;
    [EstateLocalizationFields.LONGITUDE]: number;
}
