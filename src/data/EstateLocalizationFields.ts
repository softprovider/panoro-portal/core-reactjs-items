export enum EstateLocalizationFields {
    LATITUDE = 'lat',
    LONGITUDE = 'lng',
}
